# FJE

一个可视化JSON文件的小工具.

## 类图

``` plantuml
@startuml
left to right direction
skinparam linetype polyline
skinparam linetype ortho

package display_style {
    abstract DisplayStyle {
        +Fn::call(icon: &IconFamily, value: &Value) -> Vec<String>
    }
    entity rect_style as "rect_style(icon: &IconFamily, value: &Value) -> Vec<String>"
    entity tree_style as "tree_style(icon: &IconFamily, value: &Value) -> Vec<String>"
    rect_style --|> DisplayStyle
    tree_style --|> DisplayStyle
}

package icon_family {
    struct IconFamily {
        +null: String
        +bool: String
        +number: String
        +string: String
        +array: String
        +object: String
        ---
        +{static}from_cats(inner: &str, leaf: &str) -> Self
        +get_icon(node: &Value) -> &str
    }
    note right of IconFamily
    我们简单地用一组数据实现图标, 因为图标基本只和数据类型有关.
    事实上, 大多数时候, 图标甚至只和节点是否有子节点有关.
    end note

    entity new_factory_from_config as "new_factory_from_config(conf: impl Read) -> serde_json::Result<impl Fn(&str) -> impl IconFamilyFactory>"

    entity icon_none as "icon_none() -> IconFamily"
    entity icon_circ as "icon_circ() -> IconFamily"
    entity icon_star_circ as "icon_star_circ() -> IconFamily"
    note "几个返回图标的函数, 由于Rust语言的限制, 它们不能直接定义为单例" as icon_fn
    icon_fn .. icon_none
    icon_fn .. icon_circ
    icon_fn .. icon_star_circ

    abstract icon_family.IconFamilyFactory {
        Fn::call() -> IconFamily
    }
    IconFamilyFactory --> IconFamily: 创建
    icon_none --|> IconFamilyFactory: 实现
    icon_circ --|> IconFamilyFactory: 实现
    icon_star_circ --|> IconFamilyFactory: 实现
    new_factory_from_config ..> IconFamilyFactory: 创建
}

package cli {
    class Cli {
        -styles: HashMap<String, Box<dyn DisplayStyle>>
        -icons: HashMap<String, IconFamily>
        -default_style: String
        -default_icon: String
        ---
        +Debug::fmt(&mut Formatter) -> fmt::Result
        ---
        +{static} Default::default() -> Self
        ---
        +{static}new() -> Self
        +run(args: Vec<String>) -> Result<()>
    }
    note top of Cli
    主要的CLI接口, 用于方便用户拓展CLI.

    我们的CLI程序也只是对这一接口的简单封装.
    end note

    class Builder {
        +Debug::fmt(&mut Formatter) -> fmt::Result
        ---
        +{static} Default::default() -> Self
        ---
        +{static} new() -> Self
        +{static} preset() -> Self
        ---
        +style(name: String, value: impl DisplayStyle + 'static) -> Self
        +icon(name: String, value: IconFamily) -> Self
        +extend_style(styles: impl IntoIterator<Item = (String, Box<dyn DisplayStyle>)>) -> Self
        +extend_icon(icons: impl IntoIterator<Item = (String, IconFamily)>) -> Self
        +default_style(style: String) -> Self
        +default_icon(icon: String) -> Self
        ---
        +build() -> Option<Cli>
    }

    display_style.DisplayStyle ..> IconFamily: 使用
    Cli ..> display_style.DisplayStyle: 使用
    Cli ..> IconFamily: 使用
    Builder --> Cli: 创建
}

@enduml
```

## 设计说明

我们使用的 `serde_json` 库的 `Value` 实现了迭代器模式, 使得我们可以用迭代器访问json树中的元素.

我们的display_style模块使用了策略模式: 我们直接传递函数作为策略的实现, 并且使用Fn trait来为我们的策略提供接口.
