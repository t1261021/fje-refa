use std::{collections::HashMap, io::Read};

use serde::{Deserialize, Serialize};
use serde_json::Value;

/// Icons for the nodes, usually displayed before the label
///
/// You may want to add a space after each icon
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct IconFamily {
    /// Icon for null nodes
    pub null: String,
    /// Icon for bool nodes
    pub bool: String,
    /// Icon for number nodes
    pub number: String,
    /// Icon for string nodes
    pub string: String,
    /// Icon for array nodes
    pub array: String,
    /// Icon for object nodes
    pub object: String,
}

impl IconFamily {
    /// Conveniently create an icon family from node categories
    #[must_use]
    pub fn from_cats(inner: &str, leaf: &str) -> Self {
        Self {
            null: leaf.to_string(),
            bool: leaf.to_string(),
            number: leaf.to_string(),
            string: leaf.to_string(),
            array: inner.to_string(),
            object: inner.to_string(),
        }
    }

    /// Get corresponding icon of the node
    #[must_use]
    pub fn get_icon<'a>(&'a self, node: &Value) -> &'a str {
        match node {
            Value::Null => &self.null,
            Value::Bool(_) => &self.bool,
            Value::Number(_) => &self.number,
            Value::String(_) => &self.string,
            Value::Array(_) => &self.array,
            Value::Object(_) => &self.object,
        }
    }
}

/// No icon for all items
#[must_use]
pub fn icon_none() -> IconFamily {
    IconFamily::from_cats("", "")
}

/// The same circle icon for all items
#[must_use]
pub fn icon_circ() -> IconFamily {
    IconFamily::from_cats("o ", "o ")
}

/// `*` for inner node and `o` for leaf
#[must_use]
pub fn icon_star_circ() -> IconFamily {
    IconFamily::from_cats("* ", "o ")
}

/// Create an factory from specified config file
pub fn new_factory_from_config(conf: impl Read) -> serde_json::Result<impl Fn(&str) -> impl IconFamilyFactory> {
    let conf: HashMap<String, IconFamily> = serde_json::from_reader(conf)?;
    Ok(move |name: &str| { let res = conf[name].clone(); move || res.clone() })
}

/// Abstract factory for icon families
pub trait IconFamilyFactory: Fn() -> IconFamily {}
impl<T: Fn() -> IconFamily> IconFamilyFactory for T {}
