/*!
 * Representing abstract trees
 */

use serde_json::Value;

/// Representing an abstract tree structure
pub trait AbsTree {
    /// Whether this is a leaf node
    fn is_leaf(&self) -> bool;
    /// Rendered text of this node
    fn text(&self) -> Option<String>;
    /// Iterate the children of this node
    fn children<'a>(&'a self) -> Box<dyn Iterator<Item = (Option<&str>, Box<dyn AbsTree + 'a>)> + 'a>;
    /// Obtain the corresponding json value
    fn value(&self) -> &Value;
}

/// The root node of JSON
#[derive(Debug, Clone)]
pub struct JsonRoot<'a> {
    val: &'a Value,
}

fn mkjson<'a>(val: &'a Value) -> Box<dyn AbsTree + 'a> {
    match val {
        Value::Null | Value::Bool(_) | Value::Number(_) | Value::String(_) => Box::new(JsonLeaf { val }),
        Value::Array(_) | Value::Object(_) => Box::new(JsonInner { val }),
    }
}

fn json_children<'a>(val: &'a Value) -> Box<dyn Iterator<Item = (Option<&str>, Box<dyn AbsTree + 'a>)> + 'a> {
    match val {
        Value::Null | Value::Bool(_) | Value::Number(_) | Value::String(_) => {
            Box::new(std::iter::once((None, mkjson(val))))
        }
        Value::Array(arr) => Box::new(arr.iter().map(|v| (None, mkjson(v)))),
        Value::Object(obj) => Box::new(obj.iter().map(|(k, v)| (Some(&**k), mkjson(v)))),
    }
}

impl AbsTree for JsonRoot<'_> {
    fn is_leaf(&self) -> bool {
        false
    }

    fn text(&self) -> Option<String> {
        None
    }

    fn children<'a>(&'a self) -> Box<dyn Iterator<Item = (Option<&str>, Box<dyn AbsTree + 'a>)> + 'a> {
        json_children(self.val)
    }

    fn value(&self) -> &Value {
        self.val
    }
}

impl<'a> JsonRoot<'a> {
    /// Create JSON tree from JSON value
    pub fn new(val: &'a Value) -> Self {
        Self { val }
    }
}

/// An inner node of JSON
#[derive(Debug, Clone)]
pub struct JsonInner<'a> {
    val: &'a Value,
}

impl AbsTree for JsonInner<'_> {
    fn is_leaf(&self) -> bool {
        false
    }

    fn text(&self) -> Option<String> {
        None
    }

    fn children<'a>(&'a self) -> Box<dyn Iterator<Item = (Option<&str>, Box<dyn AbsTree + 'a>)> + 'a> {
        json_children(self.val)
    }

    fn value(&self) -> &Value {
        self.val
    }
}

/// A leaf of JSON
#[derive(Debug, Clone)]
pub struct JsonLeaf<'a> {
    val: &'a Value,
}

impl AbsTree for JsonLeaf<'_> {
    fn is_leaf(&self) -> bool {
        true
    }

    fn text(&self) -> Option<String> {
        Some(match self.val {
            Value::Null => "null".to_string(),
            Value::Bool(b) => b.to_string(),
            Value::Number(n) => n.to_string(),
            Value::String(s) => format!("{s:?}"),
            Value::Array(_) | Value::Object(_) => unreachable!(),
        })
    }

    fn children<'a>(&'a self) -> Box<dyn Iterator<Item = (Option<&str>, Box<dyn AbsTree + 'a>)> + 'a> {
        Box::new(std::iter::empty())
    }

    fn value(&self) -> &Value {
        self.val
    }
}
