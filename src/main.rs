use std::env;

use anyhow::Result;
use fje::cli::Cli;

fn main() -> Result<()> {
    Cli::default().run(env::args().collect())
}
