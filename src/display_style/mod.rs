use serde_json::Value;

use crate::icon_family::IconFamily;

/**
 * The concrete display function.
 */
pub trait DisplayStyle = Fn(&IconFamily, &Value) -> Vec<String>;

/**
 * The builtin rectangle style, can be replaced.
 *
 * See the document of [`RectStyle`]().
 */
mod rect_style;
/**
 * The builtin tree style, can be replaced.
 *
 * See the document of [`TreeStyle`]().
 */
mod tree_style;
pub use rect_style::rect_style;
pub use tree_style::tree_style;
